class Pal < Formula
  desc "Simple tool that prints the content of a specified line number"
  homepage "https://gitlab.com/ComicSads/pick-a-line/"
  url "https://gitlab.com/ComicSads/pick-a-line/uploads/24409525a3397d37d958755ec641cdf1/pick-a-line-0.1.tgz"
  sha256 "54e3fd77fe141efdf8d5cddde8c5ea604fce1f895add4264444a9496d5827e06"

  depends_on "go" => :build

  def install
  # Set gopath
  ENV["GOPATH"] = buildpath
  bin_path = buildpath/"src/gitlab.com/ComicSads/pick-a-line"

  # Download files and build
  bin_path.install Dir["*"]
  cd bin_path do
    system "go", "get", "-d", "./..."
    system "go", "build", "-o", bin/"pal", "."
  end
  end
  test do
    assert_match "pal version 1.0", shell_output("#{bin}/pal version 2>&1", 2)
  end
end
