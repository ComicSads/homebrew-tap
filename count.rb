class Count < Formula
  desc "Counts down from specified number and doesn't clog terminal screen"
  homepage "https://gitlab.com/ComicSads/count/"
  url "https://gitlab.com/ComicSads/count/uploads/4adeadfc4e4b8bb9c2767e9f5463237d/count-1.0.tgz"
  sha256 "59a09233181640ffdb1a834b164b9a50429fcd03f32caf4751cccabf7904a90e"

  depends_on "go" => :build

  def install
  # Set gopath
  ENV["GOPATH"] = buildpath
  bin_path = buildpath/"src/gitlab.com/ComicSads/count"

  # Download files and build
  bin_path.install Dir["*"]
  cd bin_path do
    system "go", "get", "-d", "./..."
    system "go", "build", "-o", bin/"count", "."
  end
  end
  test do
    assert_match "count version 1.0", shell_output("#{bin}/count version 2>&1", 2)
  end
end
