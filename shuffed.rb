class Shuffed < Formula
  desc "Program that outputs a random line from a file"
  homepage "https://gitlab.com/ComicSads/shuffed/"
  url "https://gitlab.com/ComicSads/shuffed/uploads/72cda6f390456f1239b5e1c894136ed1/shuffed-0.1.tgz"
  sha256 "689ea3890a0f3a1840f68dc3ffac9429bd089ebc643e2fad5375c4a909acd495"

  depends_on "go" => :build

  def install
  # Set gopath
  ENV["GOPATH"] = buildpath
  bin_path = buildpath/"src/gitlab.com/ComicSads/shuffed"

  # Download files and build
  bin_path.install Dir["*"]
  cd bin_path do
    system "go", "get", "-d", "./..."
    system "go", "build", "-o", bin/"shuffed", "."
  end
  end
  test do
    assert_match "shuffed version 1.0", shell_output("#{bin}/shuffed version 2>&1", 2)
  end
end
