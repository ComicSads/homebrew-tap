class Nekoprint < Formula
  desc "Neko is an echo replacement that allows for formatted strings like \\n"
  homepage "https://gitlab.com/ComicSads/neko/"
  url "https://gitlab.com/ComicSads/neko/uploads/2b71a15e23445380e90090798b0cd5b9/neko-1.1.tgz"
  sha256 "34facded8d010a7c140536ad2d6feae117fb86b7073f1d56ac7cfe5493d139da"

  depends_on "go" => :build

  def install
  # Set gopath
  ENV["GOPATH"] = buildpath
  bin_path = buildpath/"src/gitlab.com/ComicSads/neko"

  # Download files and build
  bin_path.install Dir["*"]
  cd bin_path do
    system "go", "get", "-d", "./..."
    system "go", "build", "-o", bin/"neko", "."
  end
  end
  test do
    assert_match "neko version 1.1", shell_output("#{bin}/neko version 2>&1", 2)
  end
end
