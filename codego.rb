class Codego < Formula
  desc "Allows you to edit go files while excluding any other files"
  homepage "https://gitlab.com/ComicSads/codego/"
  url "https://gitlab.com/ComicSads/codego/uploads/21cc6e4ba3179ae5217776a28ab29f4e/codego-1.0.tgz"
  sha256 "5921f1339d01ccd39557799c8cf979ce0ac1da3ac3bfb8d58f5e659b7bf4d069"

  depends_on "go" => :build

  def install
  # Set gopath
  ENV["GOPATH"] = buildpath
  bin_path = buildpath/"src/gitlab.com/ComicSads/codego"

  # Download files and build
  bin_path.install Dir["*"]
  cd bin_path do
    system "go", "get", "-d", "./..."
    system "go", "build", "-o", bin/"codego", "."
  end
  end
  test do
    assert_match "codego version 1.0", shell_output("#{bin}/codego version 2>&1", 2)
  end
end
